/**
 * CS 5133: Data Networks
 * Project 1
 *
 * Author: Morgan Robertson
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netinet/in.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <unistd.h>

void handleClient(int connSock, struct sockaddr_in *connAddr, socklen_t connAddrSize);
int readWithTimeout(int connSock, int timeout_s, void *buffer, size_t bufSize);
int getBalance(const char *username, const char *password, char *balance,
                size_t balanceSize);
void strip(char *s);

int main(int argc, char *argv[]) {
    in_port_t serverPort = 0;
    int serverSock = 0;
    int connSock = 0;
    int status = 0;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <port>\n", argv[0]);
        exit(1);
    }

    serverPort = atoi(argv[1]);

    // Address information for this server
    struct sockaddr_in serverAddr;
    socklen_t addrSize = sizeof(serverAddr);
    memset(&serverAddr, 0, addrSize);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(serverPort);
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    // Address information for a client connection
    struct sockaddr_in connAddr;
    
    // Create a socket for incoming connections
    serverSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (-1 == serverSock) {
        fprintf(stderr, "Failed to create socket\n");
        exit(1);
    }

    // Bind the server socket
    status = bind(serverSock, (struct sockaddr *)&serverAddr, addrSize);
    if (-1 == status) {
        fprintf(stderr, "Failed to bind to socket\n");
        exit(1);
    }

    // Begin listening for incoming connections
    status = listen(serverSock, 5);
    if (-1 == status) {
        fprintf(stderr, "Failed to listen on socket\n");
        exit(1);
    }

    // Accept incoming connections
    while(1) {
        socklen_t connAddrSize = sizeof(connAddr);
        memset(&connAddr, 0, connAddrSize);

        connSock = accept(serverSock, (struct sockaddr *)&connAddr, &connAddrSize);
        if (-1 == connSock) {
            fprintf(stderr, "Failed to accept connection\n");
            exit(1);
        }

        // Fork a process to handle the connection
        pid_t pid = fork();
        if (pid < 0) {
            fprintf(stderr, "Fork error\n");
            exit(1);
        }
        else if (0 == pid) {
            // Child process
            handleClient(connSock, &connAddr, connAddrSize);
            break;
        }
    }
}

/**
 * Process a client. The username and password are obtained from the client,
 * then the balance is looked up and sent to the client.
 *
 * connSock - A file descriptor for the client socket.
 * connAddr - A pointer to the client's address information.
 * connAddrSize - The size of connAddr.
 */
void handleClient(int connSock, struct sockaddr_in *connAddr, socklen_t connAddrSize) {
    int bytesRead = 0;
    int bytesWritten = 0;
    int status = 0;
    char username[256] = "";
    char password[256] = "";
    char balance[256] = "";
    char writeBuf[256] = "";

    // Read username from client
    bytesRead = readWithTimeout(connSock, 30, username, sizeof(username));
    strip(username);

    // Send acknowledgement
    const char ack[] = "Username received\n";
    bytesWritten = write(connSock, ack, strlen(ack));
    if (bytesWritten < strlen(ack)) {
        fprintf(stderr, "Write error\n");
        close(connSock);
        return;
    }

    // Read password from client
    bytesRead = readWithTimeout(connSock, 30, password, sizeof(password));
    strip(password);

    // Attempt to look up the balance
    if (0 != getBalance(username, password, balance, sizeof(balance))) {
        strcpy(writeBuf, "Incorrect username and/or password");
    }
    else {
        strcpy(writeBuf, "Balance: ");
        strncat(writeBuf, balance, sizeof(writeBuf) - 10);
    }

    // Send the balance or error message to the client
    bytesWritten = write(connSock, writeBuf, strlen(writeBuf));
    if (bytesWritten < strlen(writeBuf)) {
        fprintf(stderr, "Write error\n");
        close(connSock);
        return;
    }

    close(connSock);
}

/**
 * Read from a connected socket with a timeout.
 *
 * connSock - A file descriptor for the connected socket.
 * timeout_s - The timeout in seconds.
 * buffer - A buffer to store read data.
 * bufSize - The size of buffer.
 */
int readWithTimeout(int connSock, int timeout_s, void *buffer, size_t bufSize) {
    int bytesRead = 0;
    int nReady = 0;
    fd_set read_set;
    struct timeval timeout;
    timeout.tv_sec = timeout_s;
    timeout.tv_usec = 0;

    FD_SET(connSock, &read_set);

    // Wait for message with timeout
    nReady = select(connSock + 1, &read_set, NULL, NULL, &timeout);
    if (-1 == nReady) {
        fprintf(stderr, "Read error\n");
        close(connSock);
        exit(1);
    }
    else if (0 == nReady) {
        fprintf(stderr, "Client connection timed out\n");
        close(connSock);
        exit(1);
    }

    bytesRead = read(connSock, buffer, bufSize);
    if (-1 == bytesRead) {
        fprintf(stderr, "Read error\n");
        close(connSock);
        exit(1);
    }

    return bytesRead;
}

/**
 * Look up the balance given a valid username and password. If the username and
 * password combination is invalid, -1 is returned, otherwise 0 is returned.
 *
 * username - The username to look up the balance for.
 * password - The password corresponding to username.
 * balance - A buffer to write the balance in.
 * balanceSize - The size of balance.
 */
int getBalance(const char *username, const char *password, char *balance,
                size_t balanceSize) {
    char *line = NULL;
    size_t len = 0;
    FILE *file = fopen("userList.txt", "r");
    char fileUsername[256] = "";
    char filePassword[256] = "";

    while (getline(&line, &len, file) > 0) {
        // Read username from file and compare to passed-in username
        char *tokenPtr = strtok(line, " \t");
        //strncat(fileUsername, tokenPtr, sizeof(fileUsername) - 1);
        if (0 != strcmp(username, tokenPtr)) {
            continue;
        }

        // Read password from file and compare to passed-in password
        tokenPtr = strtok(NULL, " \t");
        if (0 != strcmp(password, tokenPtr)) {
            memset(balance, 0, balanceSize);
            return -1;
        }

        tokenPtr = strtok(NULL, " \t\n");
        strncpy(balance, tokenPtr, balanceSize);
        strip(balance);
        return 0;
    }

    return -1;
}

/**
 * Strip trailing whitespace from a string.
 *
 * s - A string to strip.
 */
void strip(char *s) {
    size_t i;
    for (i = strlen(s) - 1; i > 0; i--) {
        if (s[i] == ' ' || s[i] == '\t' || s[i] == '\n' || s[i] == '\r') {
            s[i] = '\0';
        }
        else {
            break;
        }
    }
}
