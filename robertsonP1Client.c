/**
 * CS 5133: Data Networks
 * Project 1
 *
 * Author: Morgan Robertson
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    in_port_t serverPort = 0;
    int connSock = 0;
    int status = 0;
    int bytesWritten = 0;
    int bytesRead = 0;
    char username[256] = "";
    char password[256] = "";
    char buffer[256] = "";

    if (argc != 3) {
        fprintf(stderr, "Usage: %s <server> <port>\n", argv[0]);
        exit(1);
    }

    serverPort = atoi(argv[2]);

    // Address information for the server
    struct sockaddr_in serverAddr;
    socklen_t addrSize = sizeof(serverAddr);
    memset(&serverAddr, 0, addrSize);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(serverPort);
    inet_pton(AF_INET, argv[1], &serverAddr.sin_addr.s_addr);

    // Create a socket for connecting to server
    connSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (-1 == connSock) {
        fprintf(stderr, "Failed to create socket\n");
        exit(1);
    }

    // Connect to server
    status = connect(connSock, (struct sockaddr *)&serverAddr, addrSize);
    if (-1 == status) {
        fprintf(stderr, "Connection to server failed\n");
        exit(1);
    }

    // Get username from user
    printf("Welcome\nEnter username: ");
    scanf("%s", username);

    // Send username to server
    bytesWritten = write(connSock, username, strlen(username));
    if (bytesWritten < strlen(username)) {
        fprintf(stderr, "Error writing username\n");
        exit(1);
    }

    // Get acknowledgement from server
    bytesRead = read(connSock, buffer, sizeof(buffer));
    if (-1 == bytesRead) {
        fprintf(stderr, "Read error\n");
        exit(1);
    }

    // Get password from user
    printf("Enter password: ");
    scanf("%s", password);

    // Send password to server
    bytesWritten = write(connSock, password, strlen(password));
    if (bytesWritten < strlen(password)) {
        fprintf(stderr, "Error writing password\n");
        exit(1);
    }

    // Get response from server
    memset(buffer, 0, sizeof(buffer));
    bytesRead = read(connSock, buffer, sizeof(buffer));
    if (-1 == bytesRead) {
        fprintf(stderr, "Read error\n");
        exit(1);
    }

    printf("%s\n", buffer);
}
